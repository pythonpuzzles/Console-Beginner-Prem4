# Python Console Beginners Premium Puzzle 4
## Thank you, for contributing to the channel! 
### Your money helps support me and allows me to keep making these learning puzzles.

### View the YouTube Video: 


## Puzzles
In main.py, I have commented out some starting code for these puzzles. <br />
Write your code in these sections and uncomment them. <br />
I have provided some examples of similar functions to what each puzzle is asking for. Learn to read the code and modify it. <br />

At the very bottom, I have listed the functions. Comment and uncomment them, to run them one at a time.

```
# Run the puzzles

    example_a()
    # puzzle_a()

    example_b()
    # puzzle_b()

    example_c()
    # puzzle_c()
```

### A - Magic Numbers
Example A has "magic numbers". <br />
Numbers that may change at a later date but are hard coded into that program. <br />
Copy the program above and fix it so that the configuration section at the top contains variables for the hardcoded values. <br />


### B - Dirty Bird
Copy the example "Lickin' Chicken" program above. <br />
Change the catchphrase and increase price of the menu items due to inflation. <br />
Add in a 3rd option (of some chicken-related snack). <br />
When the user makes their selection, ask them <br />
"Do you want to SUPER-SIZE it for an extra $2? Enter 'y' / 'n':" <br />
Add 2.00 onto the total (or not) <br />


### C - Common Letters
Write a program that asks the user to enter a word <br />
If the word ends in the letter "e", "s" "d" or "t" print <br />
"The most common letter a word ends in are 'e', 's', 'd' and 't' " <br />
"Your word {word} ends in a {lastLetter}" <br />
Otherwise, tell them "Nice word...{word}" <br />
Learn about "e" double quotes, single quotes 'e' <br />
Learn how to get the last letter of a word <br />
Learn multiple ORs inside an if statement <br />


# Good Luck!