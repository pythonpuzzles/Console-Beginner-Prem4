from decimal import Decimal
import random


def example_a():
    print('\nExample A')
    print('~~~~~~~~~')

    # Config Section

    print("Tax Department Questionnaire")
    income = Decimal(input("What was your income for the tax year? :"))
    print(f"Recorded Answer: {income}")

    remaining = Decimal(input("How much do you have left?: "))
    print(f"Recorded Answer: {remaining}")

    # Could also be written as
    # if 0 < income <= 12000
    if income > 0 and income <= 12000:
        print("You are in the 10% tax bracket")
    elif income > 12000 and income <= 50000:
        print("You are in the 12% tax bracket")
    elif income > 50000 and income <= 100000:
        print("You are in the 22% tax bracket")
    elif income > 100000:
        print("You are in the 30% tax bracket")
    else:
        print("Don't lie to the tax department!")

    if remaining > 0:
        print(f"You have ${remaining} left? Send it all to us!")
    else:
        print("Probably, you owe us money then...")


# Puzzle A - Magic Numbers
#
# Example A has "magic numbers".
# Numbers that may change at a later date but are hard coded into that program.
# Copy the program above and fix it so that the configuration section at the top
# contains variables for the hardcoded values.
def puzzle_a():
    print('\nPuzzle A')
    print('~~~~~~~~~')


def example_b():
    print('\nExample B')
    print('~~~~~~~~~')

    # Config section, avoid using "Magic Numbers"
    company_name = "Lickin' Chicken"
    catchphrase = "Don't be a chicken, try our fried chicken!"
    dirty_bird_box = Decimal(19.99)
    pigeon_burger = Decimal(14.99)

    print(f"Welcome to {company_name}")
    print(f"Hey friend - {catchphrase}")

    print("\n Would you like to place an order?")
    print("1: Dirty Bird Box")
    print("2: Pigeon Burger, we mean Chicken Burger")

    choice = int(input("Please enter the number of your selection: "))

    total_cost = Decimal(0)
    if choice == 1:
        print("Great! Let me just throw it in the microwave and it'll be ready in a couple minutes!")
        total_cost = round(total_cost + dirty_bird_box, 2)
    elif choice == 2:
        print("Excellent! I'll go shoot a pigeon, I mean chicken, it won't take long...")
        total_cost = round(total_cost + pigeon_burger, 2)
    else:
        print("Error. Enter '1' or '2' or get out.")

    print(f"Cost: {total_cost}")


# Puzzle B - Dirty Bird
#
# Copy the example "Lickin' Chicken" program above.
# Change the catchphrase and increase price of the menu items due to inflation.
# Add in a 3rd option (of some chicken-related snack).
# When the user makes their selection, ask them
#   "Do you want to SUPER-SIZE it for an extra $2? Enter 'y' / 'n':"
# Add 2.00 onto the total (or not)
def puzzle_b():
    print('\nPuzzle B')
    print('~~~~~~~~~')


def example_c():
    print('\nExample C')
    print('~~~~~~~~~~~')

    word = input("\nHey, You! Enter a four letter word: ")

    if word is not None and word != "":
        # Find the length of the word with len(). How many characters there are in it.
        if len(word) != 4:
            print("Error! I said enter a FOUR letter word")
        else:
            print(f"Good job! Your word was \"{word}\" and that has 4 letters.")
    else:
        print("Error! I said enter a word")


# Puzzle C - Common Letters
#
# Write a program that asks the user to enter a word
# If the word ends in the letter "e", "s" "d" or "t" print
#   "The most common letter a word ends in are 'e', 's', 'd' and 't' "
#   "Your word {word} ends in a {last_letter}"
# Otherwise, tell them "Nice word...{word}"
# Learn how to get the last letter of a word
# Learn multiple ORs inside an if statement
def puzzle_c():
    print('\nPuzzle C')
    print('~~~~~~~~~~~')


if __name__ == '__main__':

    # Run the puzzles

    example_a()
    # puzzle_a()

    example_b()
    # puzzle_b()

    example_c()
    # puzzle_c()
